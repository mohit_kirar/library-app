var app = app || {};

$(function(){
	var books = [
		{title: 'War of the worlds', author: 'H.G.Wells', releaseDate: '1980', keywords: 'Action Horror Sci-Fi', coverImage: 'img/war_of_the_worlds.jpeg'},
		{title: '20000 Leagues under the sea', author: 'Jules Verne', releaseDate: '1982', keywords: 'Action Adventure Fantasy', coverImage: 'img/20000_leagues.jpeg'},
		{title: 'A tale of two cities', author: 'Charles Dickens', releaseDate: '1975', keywords: 'Adventure Drama', coverImage: 'img/a_tale_of_two_cities.jpeg'},
		{title: 'Frankenstein', author: 'H.G.Wells', releaseDate: '1977', keywords: 'Horror Sci-Fi', coverImage: 'img/frankenstein.jpeg'},
		{title: 'Journey to the center of the earth', author: 'Vintage Verne', releaseDate: '1988', keywords: 'Adventure Fantasy', coverImage: 'img/journey_to_the_center_of_the_earth.jpeg'},
		{title: 'The Pearl', author: 'J. Steinbeck', releaseDate: '1992', keywords: 'Mystery', coverImage: 'img/the_pearl.jpeg'},
		{title: 'The Catcher in the rye', author: 'J.D. Salinger', releaseDate: '1980', keywords: 'Mystery Thriller', coverImage: 'img/the_catcher_in_the_rye.jpeg'}
	];

	new app.LibraryView( books );
});